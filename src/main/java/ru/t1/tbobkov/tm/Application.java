package ru.t1.tbobkov.tm;

import ru.t1.tbobkov.tm.constant.ArgumentConstant;

public class Application {

    public static void main(String[] args) {
        processArgs(args);
    }

    public static void showVersion() {
        System.out.println("[version]");
        System.out.println("1.2.0");
    }

    public static void showAbout() {
        System.out.println("[about]");
        System.out.println("Name: Timur Bobkov");
        System.out.println("E-mail: tbobkov@t1-consulting.ru");
    }

    public static void showHelp() {
        System.out.println("[help]");
        System.out.printf("%s - show version info\n", ArgumentConstant.VERSION);
        System.out.printf("%s - show developer info\n", ArgumentConstant.ABOUT);
        System.out.printf("%s - show this list of commands\n", ArgumentConstant.HELP);
    }

    public static void showError() {
        System.out.println("[error]");
        System.out.println("incorrect input, type 'help' to see the list of commands");
    }

    public static void processArgs(final String [] args) {
        if (args == null || args.length < 1) {
            showError();
            return;
        }
        processArg(args[0]);
    }

    public static void processArg(final String arg) {
        switch (arg.toLowerCase()) {
            case ArgumentConstant.VERSION:
                showVersion();
                break;
            case ArgumentConstant.ABOUT:
                showAbout();
                break;
            case ArgumentConstant.HELP:
                showHelp();
                break;
            default:
                showError();
                break;
        }
    }

}